<?php

define('APP_URL', '');
define('MYSQL_HOST', 'mysql');
define('MYSQL_USER', 'root');
define('MYSQL_PASS', 'password');
define('MYSQL_DB', 'myapp');

function registerClassDirectory($path) {
  spl_autoload_register(function($class) use ($path) {
    $file = $path.$class.'.php';
    if (file_exists($file))
      include($file);
  });
}

registerClassDirectory('models/');
registerClassDirectory('controllers/');
registerClassDirectory('helpers/');

DB::init(MYSQL_HOST, MYSQL_USER, MYSQL_PASS, MYSQL_DB);

$router = new Router($_SERVER['REQUEST_URI'], APP_URL);
$router->handle();

?>
