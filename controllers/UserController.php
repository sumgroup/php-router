<?php

class UserController {
  public function __construct() {
  }

  public function getIndex($args) {
    $id = $args['id'];

    $user = User::get($id);

    echo json_encode($user);
  }

  /* just an example */
  public function getCreate($args) {
    $name = $args['name'];

    $user = User::create($name);

    header("Location: /user/index/?id=" . $user->id);
  }
}
