<?php

DB::query(
"CREATE TABLE IF NOT EXISTS users (" .
"`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY," .
"`name` TEXT)"
);

class User {
  private function __construct($row) {
    foreach($row as $k => $v) {
      $this->$k = $v;
    }
  }
  public static function get($user_id) {
    $user = DB::fetchOne("SELECT * FROM users WHERE id=" . $user_id);
    if ($user)
      return new User($user);
    return null;
  }

  public static function create($name) {
    DB::query("INSERT INTO users (name) VALUES ('$name')");
    return self::get(DB::insertID());
  }
}
