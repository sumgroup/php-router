A very simple php router. Mostly suited to make quick and dirty app backends.

Config is done in index.php.

It sets up three folders for Controllers, Models, and Helpers.

The Router will forward any GET request to the url  on the form
/controller/action?any=get&arg=here to a call to the method get{Action} on class
{Controller}Controller with the $_REQUEST passed as the only argument.

E.g. to make an API endpoint at /user/steps/?user_id=4, then create a class
UserController (in the folder controllers preferrably), and implement the
method getSteps($args) that uses the argument 'user_id'.
