<?php

class Router {
  private $reqest;
  private $controller;
  private $action;

  public function __construct($request, $app_url) {
    $this->request = str_replace($app_url, "", $request);

    $matches = [];
    preg_match_all('/(\w+)/', $this->request, $matches );
    if ($matches) {
      $this->controller = @$matches[0][0]?:'home';
      $this->action = @$matches[0][1]?:'index';
    }
  }

  public function handle() {
    global $_SERVER;

    if (!$this->controller) {
      echo "no controller";
      exit;
    }

    $controllerClass = ucfirst($this->controller) . "Controller";
    $method = strtolower($_SERVER['REQUEST_METHOD']) . ucfirst($this->action);

    $controller = new $controllerClass();
    $controller->$method($_REQUEST);
  }
}
