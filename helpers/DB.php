<?php

class DB {
  public static $conn;

  public static function init($host, $user, $pass, $db = null) {
    self::$conn = @mysql_connect($host, $user, $pass);
    if ($db!=null)
      mysql_select_db($db, self::$conn);
  }

  public static function query($query) {
    return mysql_query($query, self::$conn);
  }

  public static function fetchRows($query) {
    $r = self::query($query);
    $rows = [];
    if ($r) {
      while($row = mysql_fetch_assoc($r)) {
        $rows[] = $row;
      }
      mysql_free_result($r);
    }
    return $rows;
  }

  public static function fetchOne($query) {
    $r = self::query($query);
    if (!$r) return null;
    $row = mysql_fetch_assoc($r);
    mysql_free_result($r);
    return $row;
  }

  public static function insertID() {
    return mysql_insert_id(self::$conn);
  }
}
